import Vue from 'vue'
import VueRouter from 'vue-router'
import ProjectDetail from '../views/it1/ProjectDetail'
import ProjectList from '../views/it1/ProjectList'

import ProjectSearchList from '../views/it1/ProjectSearchList'
import MainPage from '../views/MainPage'
import SingleSearch from '../views/it1/SingleSearch'
import RepositoryList from "@/views/it1/RepositoryList";
import RepositoryDetail from "@/views/it1/RepositoryDetail";
import RepositorySearchList from "@/views/it1/RepositorySearchList";
import SingleSearchRepository from "@/views/it1/SingleSearchRepository";
import ProjectVersionList from "@/views/it2/ProjectVersionList";
import ProjectVersionDetail from "@/views/it2/ProjectVersionDetail";
import ShowDependenceResult from "@/views/it2/ShowDependenceResult";
import DependenceSearchList from "@/views/it2/DependenceSearchList";
import SingleSearchDependence from "@/views/it2/SingleSearchDependence";
import SearchSubstituteList from "@/views/it3/SearchSubstituteList";
import FunctionSubstitute from "@/views/it3/FunctionSubstitute";


Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'home',
    component: MainPage
  },
  {
    path: '/projectList',
    name: 'ProjectList',
    component: ProjectList
  },
  {
    path: '/projectDetail',
    name: 'ProjectDetail',
    component: ProjectDetail
  },
  {
    path: '/projectSearchList',
    name: 'ProjectSearchList',
    component: ProjectSearchList
  },
  {
    path: '/SingleSearch',
    name: 'search',
    component: SingleSearch
  },
  {
    path: '/SingleSearchRepository',
    name: 'searchRepository',
    component: SingleSearchRepository
  },
  {
    path: '/repositoryList',
    name: 'RepositoryList',
    component: RepositoryList
  },
  {
    path: '/repositoryDetail',
    name: 'RepositoryDetail',
    component: RepositoryDetail
  },
  {
    path: '/repositorySearchList',
    name: 'RepositorySearchList',
    component: RepositorySearchList
  },
  //迭代二
  {//项目列表
    path: '/projectVersionList',
    name: 'ProjectVersionList',
    component: ProjectVersionList
  },
  {//项目细节列表
    path: '/projectVersionDetail',
    name: 'ProjectVersionDetail',
    component: ProjectVersionDetail
  },
  {//库迁移结果列表
    path: '/showDependenceResult',
    name: 'ShowDependenceResult',
    component: ShowDependenceResult
  },
  {//依赖库搜索
    path: '/dependenceSearchList',
    name: 'DependenceSearchList',
    component: DependenceSearchList
  },
  {//单独搜索依赖
    path: '/SingleSearchDependence',
    name: 'SingleSearchDependence',
    component: SingleSearchDependence
  },
  {
    path: '/SearchSubstituteList',
    name: 'SearchSubstituteList',
    component: SearchSubstituteList
  },
  {
    path: '/FunctionSubstitute',
    name: 'FunctionSubstitute',
    component: FunctionSubstitute
  },
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
