const { defineConfig } = require('@vue/cli-service')
module.exports = defineConfig({
  lintOnSave: false,
  transpileDependencies: true ,
  devServer: {
    //代理axios
    proxy: 'http://124.221.163.110:8082',
    //vue自己启动的端口
    port:8088
  }
})
